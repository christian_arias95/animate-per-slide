/**
 * /**
 * @file Archivo que contiene el módulo animate-per-slide
 * @namespace index
 * @module animate-per-slide
 * @author Christian Arias <cristian1995amr@gmail.com>
 * @requires module:jquery
 * @requires module:lodash
 */

const $ = require('jquery');
const _ = require('lodash');
/**
 * module.exports Contiene la clase.
 * @class module.exports
 */
module.exports = {

    init: function(){
        
    },
    setAnimationClasses: function(args){
        console.log('args', args);
        if(!args || !args.$slide){
            throw new Error("A slide was expected.");
        }
        if(!args.$slide instanceof jQuery){
            throw new Error("The slide is not an instance of jQuery.");
        }
        const $elems = args.$slide.find('[data-ani-class]');
        if(!$elems.length){
            return false;
        }

        if(!args.action || !args.action.length || typeof args.action !== 'string'){
            throw new Error("Invalid action.");
        }

        $elems.each(function(){
            var $this = $(this);
            var classes = $this.attr('data-ani-class').split(' ');
            if(classes.length){
                $this.addClass('animated');
                _.forEach(classes, function(v, k){
                    if(args.action === 'add'){
                        var delay = $this.attr('data-ani-delay');
                        if(!!delay){
                            (function($slide, c){
                                setTimeout(function(){
                                    $slide.addClass(c);
                                }, delay);
                            })($this, v);
                        }
                        else{
                            $this.addClass(v);
                        }
                    }
                    else{
                        if(args.action === 'remove'){
                            $this.removeClass(v);
                        }
                    }
                });
            }
        });
    },
    setAnimationDurations: function(args){
        if(!args || !args.$stage){
            throw new Error("A stage was expected.");
        }
        if(!args.$stage instanceof jQuery){
            throw new Error("The stage is not an instance of jQuery.");
        }
        const $elems = args.$stage.find('[data-ani-class]');
        if(!$elems.length){
            return false;
        }

        console.log('$elements', $elems);

        $elems.each(function(){
            var $this = $(this);
            var duration = $this.attr('data-ani-duration');
            if(duration !== 'undefined' && duration !== ''){
                if(+duration >= 0){
                    $this.css({
                        '-webkit-animation-duration': duration + 's',
                        'animation-duration': duration + 's'
                    });
                }
            }
        });
    },
};